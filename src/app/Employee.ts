export class Employee {
  // @ts-ignore
  id: number;
  // @ts-ignore
  name: string;
  // @ts-ignore
  contact: string;
  // @ts-ignore
  email: string;
  // @ts-ignore
  type: string;
  // @ts-ignore
  position: string;
  // @ts-ignore
  username: string;
  // @ts-ignore
  password: string;
}
