import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefualtComponent } from './layouts/defualt/defualt.component';
import { DefualtModule } from './layouts/defualt/defualt.module';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import {PostsComponent} from './modules/posts/posts.component';
import {RegisterComponent} from './modules/register/register.component';
import {AppointComponent} from './modules/appoint/appoint.component';
import {LoginComponent} from './modules/login/login.component';
import {PaymentComponent} from './modules/payment/payment.component';
import {EmployeeComponent} from './layouts/employee/employee.component';
import {EmployeeDashComponent} from './modules/employee-dash/employee-dash.component';
import {LeaveComponent} from './modules/leave/leave.component';
import {RequisitionComponent} from './modules/requisition/requisition.component';
import {RequisitionEmployeeComponent} from './modules/requisition-employee/requisition-employee.component';
import {AuthGuardService} from './Authentication/auth-guard.service';
import {RoleGuardServiceService} from './Authentication/role-guard-service.service';
import {Step1Component} from './modules/leave/step1/step1.component';
import {Step2Component} from './modules/leave/step2/step2.component';
import {Step3Component} from './modules/leave/step3/step3.component';
import {DumpPageComponent} from './dump-page/dump-page.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'},
  {
  path: 'admin/:id',
  component: DefualtComponent,
 // canActivate: [AuthGuardService, RoleGuardServiceService],
 // canActivate: [AuthGuardService],
    data: {
      expectedRole: 'admin'
    },
  children: [{
    path: 'ad',
    component: DashboardComponent
  },
    {
    path: '',
    redirectTo: 'ad',
    pathMatch: 'full'},
    {
    path: 'posts',
    component: PostsComponent
  },
    {
      path: 'register',
      component: RegisterComponent
    },
    {
      path: 'appoint',
      component: AppointComponent
    },
    {
      path: 'payment',
      component: PaymentComponent
    },
    {
      path: 'requisition',
      component: RequisitionComponent
    }]
},
  {
    path: 'employee/:id',
    component: EmployeeComponent,
    // canActivate: [AuthGuardService, RoleGuardServiceService],
    canActivate: [AuthGuardService],
    data: {
      expectedRole: 'user'
    },
    children: [{
      path: 'ed',
      component: EmployeeDashComponent
    },
      {
        path: '',
        redirectTo: 'ed',
        pathMatch: 'full'},
      {
        path: 'leave',
        component: LeaveComponent,
        canActivate: [AuthGuardService],
        children: [
          {path: 'step1', component: Step1Component},
          {path: 'step2', component: Step2Component},
          {path: 'step3', component: Step3Component},
          ]
      }
      ,
      {
         path: 'requisition',
        component: RequisitionEmployeeComponent
      }]
  },
  // {path: '**', redirectTo: 'login', pathMatch: 'full'}
  {path: '**', component: DumpPageComponent}
  ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
