import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import jwtDecode from 'jwt-decode';
import {findLast} from '@angular/compiler/src/directive_resolver';
@Injectable({
  providedIn: 'root'
})
export class RoleGuardServiceService {

  constructor(private AuthService: AuthenticationService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
       const expectedRole = route.data.expectedRole;
       const token = localStorage.getItem('Token');
       // @ts-ignore
       const  decodedToken = jwtDecode(token);
       // @ts-ignore
       if ( !this.AuthService.isAuthenticated() || decodedToken.role !== expectedRole){
         this.router.navigate(['login']);
         return false;
       }else{
         return true;
       }
  }

  // @ts-ignore
  checkRole(token): boolean {
   // const token = localStorage.getItem('Token');
    if (typeof token === 'string' && token !== null && token !== undefined) {
      const twn = jwtDecode(token);
      // @ts-ignore
      if (twn.role === 'admin') {
        return true;
      } else {
        return false;
      }
      /*
      if (!this.AuthService.isAuthenticated()){
        this.router.navigate(['login']);
      }else{
        // @ts-ignore
        if (dcdtwn.role === 'admin'){
           return true;
        }else{
          return false;
        }
       */
      }
    }
}
