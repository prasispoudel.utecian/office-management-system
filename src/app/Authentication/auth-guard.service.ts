import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {JwtHelperService} from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router, private jwtHelper: JwtHelperService) {
     this.canActivate();
  }
  /*
  // tslint:disable-next-line:max-line-length
  constructor(private router: Router) {
    this.canActivate();
  }
   */

  canActivate(): boolean {
    /*
    if (!this.AuthService.isAuthenticated()){
         this.router.navigate(['login']);
         return false;
    }else{
      return true;
    }
  }
     */
     const token = localStorage.getItem('Token');
     // @ts-ignore
     if (!this.jwtHelper.isTokenExpired(token)){
        return false;
     }else{
       this.router.navigate(['**']);
       return true;
     }
  }
}
