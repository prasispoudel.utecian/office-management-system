import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import {File} from '@angular/compiler-cli/src/ngtsc/file_system/testing/src/mock_file_system';
@Injectable({
  providedIn: 'root'
})
export class EmployeeRegistrationServiceService {
  private url = 'localhost:8080/api/EmployeeManagementApplication/';
  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:ban-types
  Register(Employee: Object): Observable<Object>{
    console.log(Employee);
    return this.http.post(`${this.url}` + 'add', Employee);
  }
  /*
  onUpload(SelectedFile: File): Observable<File>{
   const uploadImagedata = new FormData();
   uploadImagedata.append('Employee Image', SelectedFile, SelectedFile.name);
  }
   */

}

