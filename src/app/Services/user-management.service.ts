import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Employee} from '../Employee';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService {
  url = 'http://localhost:8080/getUser';
  constructor(private http: HttpClient) { }


  // @ts-ignore
  getUserDetail(id: number): Observable<Employee>{
     // @ts-ignore
    const tokenStr = 'Bearer ' + localStorage.getItem('Token');
    // const headers  = new HttpHeaders().set('Authorization', tokenStr);
    const opts = {
      headers: new HttpHeaders({
        Authorization : tokenStr
      })
    };
    // @ts-ignore
    return this.http.get(`${this.url}/${id}`, opts);
  }
}
