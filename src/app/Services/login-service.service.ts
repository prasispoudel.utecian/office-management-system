import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  url = 'http://localhost:8080/find';
  // tslint:disable-next-line:no-shadowed-variable
  constructor(private http: HttpClient) { }
  // @ts-ignore
  // tslint:disable-next-line:typedef
  public generateToken(request) {
    return this.http.post('http://localhost:8080/authenticate', request, {responseType: 'text' as 'json'});
  }


  // @ts-ignore
  // tslint:disable-next-line:typedef
  public welcome(token){
     const tokenStr = 'Bearer' + token;
     // const headers  = new HttpHeaders().set('Authorization', tokenStr);
     const opts = {
      headers : new HttpHeaders({
        Authorization : tokenStr
      })
     };
     return this.http.get('http://localhost:8080/welcome', opts);
  }

  // @ts-ignore
  // tslint:disable-next-line:ban-types
  getID(username: string): Observable<any>{
    const tokenStr = 'Bearer ' + localStorage.getItem('Token');
    // const headers  = new HttpHeaders().set('Authorization', tokenStr);
    /*
    headers.Add("Access-Control-Allow-Origin", "*");
    headers.Add("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS");
    headers.Add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
     */
   // console.log(username);
    // @ts-ignore
   // return this.http.get(`${this.url}/${username}`, headers, {responseType: 'text' as 'json'});
   // return this.http.get(`${this.url}/${username}`, headers);
    const opts = {
     headers : new HttpHeaders({
       Authorization : tokenStr
     })
   };
   // return this.http.get('http://localhost:8080/welcome', opts);
  //  return this.http.get('http://localhost:8080/welcome', headers, {responseType: 'text' as 'json'});
    return this.http.get(`${this.url}/${username}`, opts);
  }
}
