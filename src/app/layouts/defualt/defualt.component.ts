import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {UserManagementService} from '../../Services/user-management.service';
import { Location } from '@angular/common';
import { Employee} from '../../Employee';

@Component({
  selector: 'app-defualt',
  templateUrl: './defualt.component.html',
  styleUrls: ['./defualt.component.scss']
})
export class DefualtComponent implements OnInit {
  sideBarOpen = true;
  // @ts-ignore
  users$: Observable<any>;
  // @ts-ignore
  selectedId: number;
  // @ts-ignore
  employee: Employee;
  constructor(private route: ActivatedRoute, private service: UserManagementService, private location: Location) { }

  ngOnInit(): void {
    // @ts-ignore
   // const idfromRoute = +this.route.snapshot.paramMap.get('id');
   // console.log(idfromRoute);
   // console.log('Token Used:- ' + localStorage.getItem('Token'));
   // this.service.getUserDetail(idfromRoute).subscribe(user => {
   //    this.employee = user;
   //    console.log(user);
   // });
    /*
    this.users$ = this.route.paramMap.pipe(
      switchMap(params => {
        this.selectedId = Number(params.get('id'));
        return this.service.getHeroes();
      })
    );
     */
  }
  // tslint:disable-next-line:typedef
  sideBarToggle($event: any){
    this.sideBarOpen = !this.sideBarOpen;
  }

}
