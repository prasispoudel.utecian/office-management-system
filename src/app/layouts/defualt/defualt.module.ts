import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { DefualtComponent } from './defualt.component';
import {AppRoutingModule} from '../../app-routing.module';
import {RouterModule} from '@angular/router';
import {PostsComponent} from '../../modules/posts/posts.component';
import {SharedModule} from '../../shared/shared.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import {AppointComponent} from '../../modules/appoint/appoint.component';
import {RegisterComponent} from '../../modules/register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {AccordionModule} from 'primeng/accordion';
import {MenuItem, MessageService} from 'primeng/api';
import {MenuItemContent} from 'primeng/menu';
import {PasswordModule} from 'primeng/password';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DividerModule} from 'primeng/divider';
import {SplitterModule} from 'primeng/splitter';
import {LoginComponent} from '../../modules/login/login.component';
import {CardModule} from 'primeng/card';
import {PaymentComponent} from '../../modules/payment/payment.component';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {RequisitionComponent} from '../../modules/requisition/requisition.component';
import {ToastModule} from 'primeng/toast';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {Editor, EditorModule} from 'primeng/editor';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import { ChipModule } from 'primeng/chip';
import {PickListModule} from 'primeng/picklist';
import {StepsModule} from 'primeng/steps';
import {DialogModule} from 'primeng/dialog';
@NgModule({
  declarations: [
    DefualtComponent,
    DashboardComponent,
    PostsComponent,
    AppointComponent,
    RegisterComponent,
    PaymentComponent,
    LoginComponent,
    RequisitionComponent,
  ],
  exports: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatCardModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    AccordionModule,
    PasswordModule,
    ButtonModule,
    InputTextModule,
    DividerModule,
    SplitterModule,
    CardModule,
    TableModule,
    DropdownModule,
    ToastModule,
    ConfirmDialogModule,
    HttpClientModule,
    InputTextareaModule,
    EditorModule,
    FormsModule,
    CKEditorModule,
    ChipModule,
    PickListModule,
    StepsModule,
    DialogModule
  ]
})
export class DefualtModule { }
