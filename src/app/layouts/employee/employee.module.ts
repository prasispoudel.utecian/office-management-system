import {NgModule, ViewEncapsulation} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from '../../modules/dashboard/dashboard.component';
import {PostsComponent} from '../../modules/posts/posts.component';
import {AppointComponent} from '../../modules/appoint/appoint.component';
import {RegisterComponent} from '../../modules/register/register.component';
import {PaymentComponent} from '../../modules/payment/payment.component';
import {LoginComponent} from '../../modules/login/login.component';
import {EmployeeComponent} from './employee.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import {ReactiveFormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {AccordionModule} from 'primeng/accordion';
import {PasswordModule} from 'primeng/password';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DividerModule} from 'primeng/divider';
import {SplitterModule} from 'primeng/splitter';
import {CardModule} from 'primeng/card';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {EmployeeDashComponent} from '../../modules/employee-dash/employee-dash.component';
import {LeaveComponent} from '../../modules/leave/leave.component';
import {RequisitionComponent} from '../../modules/requisition/requisition.component';
import {RequisitionEmployeeComponent} from '../../modules/requisition-employee/requisition-employee.component';
import {MatMenuModule} from '@angular/material/menu';
import {StepsModule} from 'primeng/steps';
import {MenuItem} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import { ChipModule } from 'primeng/chip';
import {PickListModule} from 'primeng/picklist';
import {DialogModule} from 'primeng/dialog';
@NgModule({
  declarations: [
    EmployeeComponent,
    EmployeeDashComponent,
    LeaveComponent,
    RequisitionEmployeeComponent,

  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    FlexLayoutModule,
    MatCardModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    AccordionModule,
    PasswordModule,
    ButtonModule,
    InputTextModule,
    DividerModule,
    SplitterModule,
    CardModule,
    TableModule,
    DropdownModule,
    MatMenuModule,
    StepsModule,
    ToastModule,
    CKEditorModule,
    ChipModule,
    PickListModule,
    StepsModule,
    DialogModule
  ]
})
export class EmployeeModule { }
