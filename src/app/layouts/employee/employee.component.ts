import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {UserManagementService} from '../../Services/user-management.service';
import {Location} from '@angular/common';
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  sideBarOpen = true;
  constructor(private route: ActivatedRoute, private service: UserManagementService, private location: Location) { }

  ngOnInit(): void {
    // @ts-ignore
    const id = +this.route.snapshot.paramMap.get('id');
    console.log(id);
  }
  // tslint:disable-next-line:typedef
  sideBarToggle($event: any){
    this.sideBarOpen = !this.sideBarOpen;
  }
}
