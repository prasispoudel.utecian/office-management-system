import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  userid: any;
  username: any;
  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');
    this.username = localStorage.getItem('username');
  }

}
