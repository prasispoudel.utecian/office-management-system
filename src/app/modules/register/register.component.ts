import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Employee} from '../../Employee';
import {EmployeeRegistrationServiceService} from '../../Services/employee-registration-service.service';
import {ConfirmEventType, MessageService} from 'primeng/api';
import {ConfirmationService} from 'primeng/api';
import {HttpClient} from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class RegisterComponent implements OnInit {
  name = new FormControl('');
  contact = new FormControl('');
  email = new FormControl('');
  type = new FormControl('');
  position = new FormControl('');
  username = new FormControl('');
  password = new FormControl('');
  employee: Employee = new Employee();
  data: any;
  selectedImage: any;
  url = 'assets/User_icon.png';
  // @ts-ignore
  selectedFile: File;
  constructor(private massageService: MessageService, private confirmationService: ConfirmationService,
              private httpClient: HttpClient, private route: ActivatedRoute) { }
  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  // @ts-ignore
  // tslint:disable-next-line:typedef
  onselectImage(e){
    if (e.target.files){
       const reader = new FileReader();
       reader.readAsDataURL(e.target.files[0]);
       // @ts-ignore
       this.selectedFile = e.target.files[0];
       console.log(this.selectedFile);
       reader.onload = (event: any) => {
         this.url = event.target.result;
       };
    }
}
// tslint:disable-next-line:typedef
showSuccess(){
  // this.massageService.clear();
  // this.massageService.add({key: 'c', sticky: true, severity: 'warn', summary: 'Are you sure?', detail: 'Confirm to proceed'});
}

  // @ts-ignore
  // @ts-ignore
  // tslint:disable-next-line:typedef
  confirm() {
    this.massageService.add({severity: 'info', summary: 'Confirmed', detail: 'New Employee Registered'});
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.employee.name = this.name.value;
        this.employee.contact = this.contact.value;
        this.employee.email = this.email.value;
        this.employee.type = this.type.value;
        this.employee.position = this.position.value;
        this.employee.username = this.username.value;
        this.employee.password = this.password.value;
        const UploadImageData = new FormData();
        UploadImageData.append('imageFile', this.selectedFile, this.selectedFile.name);
        /*
        // tslint:disable-next-line:max-line-length
        this.httpClient.post('http//localhost:9192/api/OfficeManagement/registering', UploadImageData,
          {observe: 'response'}).subscribe((responce) => {
          if (responce.status === 200){
            console.log('Image Upload Successfully');
          }else {
            console.log('Image Not Uploaded');
          }
        });
         */
        this.massageService.add({severity: 'info', summary: 'Confirmed', detail: 'New Employee Registered'});
      },
      reject: (type: any) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this.massageService.add({severity: 'error', summary: 'Operation Aborted', detail: 'Employee Registration Aborted'});
            break;
          case ConfirmEventType.CANCEL:
            this.massageService.add({severity: 'warn', summary: 'Cancelled', detail: 'Employee Registration Aborted'});
            break;
        }
      }
    });
  }

  // tslint:disable-next-line:typedef
  onUpload(){

  }
  // tslint:disable-next-line:typedef
  register(){
    /*
     this.employee.name = this.name.value;
     this.employee.contact = this.contact.value;
     this.employee.email = this.email.value;
     this.employee.type = this.type.value;
     this.employee.position = this.position.value;
     this.employee.username = this.username.value;
     this.employee.password = this.password.value;

     */
    /*
     this.service.Register(this.employee).subscribe((data) => {
       console.log(data);
     });

      */

  }
}
