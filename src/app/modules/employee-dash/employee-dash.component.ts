import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-employee-dash',
  templateUrl: './employee-dash.component.html',
  styleUrls: ['./employee-dash.component.scss']
})
export class EmployeeDashComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
  userid: any;
  username: any;
  ngOnInit(): void {
    this.userid = localStorage.getItem('userid');
    this.username = localStorage.getItem('username');
  }

}
