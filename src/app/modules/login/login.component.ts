import {Component, OnInit} from '@angular/core';
import {LoginServiceService} from '../../Services/login-service.service';
import {FormControl} from '@angular/forms';
import {Login} from '../../Login';
import {ActivatedRoute, Router} from '@angular/router';
import jwtDecode from 'jwt-decode';
import {RoleGuardServiceService} from '../../Authentication/role-guard-service.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private service: LoginServiceService, private route: ActivatedRoute, private router: Router, private roleGuardService: RoleGuardServiceService) { }
  userName = new FormControl('');
  passWord = new FormControl('');
  user: Login = new Login();
  credentials: any;
   response: any;
  ngOnInit(): void {
    // this.generateToken(this.authRequest);
  }

  // tslint:disable-next-line:typedef
  login(){
    this.user.username = this.userName.value;
    this.user.password = this.passWord.value;
    const Authen = {
      "username": this.user.username,
      "password": this.user.password
    };
    const resp = this.service.generateToken(Authen).subscribe((data) => {
      console.log(data);
      if (typeof data === 'string') {
        localStorage.setItem('Token', data);
      }else{
        console.log('Token Not in String Format.');
      }
    });
    if (this.isLoggedIn() === true){
      const decoded = jwtDecode(localStorage.getItem('Token') as string);
      if (this.roleGuardService.checkRole(localStorage.getItem('Token'))){
        // @ts-ignore
        this.router.navigate((['admin', decoded.id]));
      }else {
        // @ts-ignore
        this.router.navigate(['user', decoded.id]);
      }
    /*
    resp.subscribe((data) => {
      console.log(data);
      if (typeof data === 'string') {
        localStorage.setItem('Token', data);
      }else{
        console.log('Token Not in String Format.');
      }
      //  this.accessApi(data);
     // console.log(localStorage.getItem('Token'));
    });
     */
    /*
    this.service.getID(this.userName.value).subscribe((data) => {
      console.log(data);
      this.user = data;
    });
     */
  }
  // tslint:disable-next-line:typedef
  // @ts-ignore
    /*
  generateToken(authRequest){
    const resp = this.service.generateToken(authRequest);
    resp.subscribe((data) => {
      console.log(data);
      if (typeof data === 'string') {
        localStorage.setItem('Token', data);
      }else{
        console.log('Token Not in String Format.');
      }
    //  this.accessApi(data);
      console.log(localStorage.getItem('Token'));
    });
    if (this.isLoggedIn() === true){
      const decoded = jwtDecode(localStorage.getItem('Token') as string);
      if (this.roleGuardService.checkRole(localStorage.getItem('Token'))){
        // @ts-ignore
        this.router.navigate((['admin', decoded.id]));
      }else {
        // @ts-ignore
        this.router.navigate(['user', decoded.id]);
         }
       // this.user.username = this.userName.value;
       // this.user.password = this.passWord.value;
      /*
        this.service.getID(this.userName.value).subscribe((data) => {
          this.user = data;
          console.log(data);
          console.log(this.user.isAdmin);
         // localStorage.setItem('username', this.user.username);
        //  localStorage.setItem('userid', this.user.id);
          console.log('Going to user with userid:-' + this.user.userid);
          if ( this.user.isAdmin === 'yes'){
           // this.router.navigate(['admin', localStorage.getItem('userid')]);
            this.router.navigate((['admin', this.user.userid]));
          }
          else if (this.user.isAdmin === 'no'){
            this.router.navigate(['employee', this.user.userid]);
          }
          else{
            console.log('User Not Found');
            // this.router.navigate(['employee', this.user.id]);
          }
        });
       // this.router.navigate(['admin', 1]);
       */
    }

    isLoggedIn(){
    const token = localStorage.getItem('Token');
    if (token == null || token === undefined ||  token === ''){
      return false;
    }else{
      return true;
    }
  }
  Logout(){
    localStorage.removeItem('Token');
    return false;
  }
  // @ts-ignore
  public  accessApi(token){
    if (this.isLoggedIn() === true){
      const res = this.service.welcome(token);
      res.subscribe((data) => {
        this.response = data;
        console.log('LoggedIN');
        console.log(data);
      });
      return true;
    }else{
      return false;
    }
  }

}
