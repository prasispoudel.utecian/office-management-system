import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {ConfirmationService, ConfirmEventType, MessageService} from 'primeng/api';
import {FormControl} from '@angular/forms';
// @ts-ignore
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {NoticeServiceService} from '../../Services/notice-service.service';
import {Post} from '../../Post';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class PostsComponent implements OnInit {

  Post = new FormControl();
  public Editor = ClassicEditor;
  post: Post = new Post();
  reply: any;
  // tslint:disable-next-line:max-line-length
  constructor(private route: ActivatedRoute, private messageService: MessageService, private confirmationService: ConfirmationService, private noticeservice: NoticeServiceService) { }

  ngOnInit(): void {
  }
  submit(){
    this.messageService.add({severity: 'info', summary: 'Confirmed', detail: 'New Notice Added to the Bulletin Board'});
    this.confirmationService.confirm({
      message: 'Are you sure that you want to publish this notice?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
       this.publishNotice();
       this.messageService.add({severity: 'info', summary: 'Ongoing process', detail: 'Publishing Notice...'});
      },
      reject: (type: any) => {
        switch (type) {
          case ConfirmEventType.REJECT:
            this.messageService.add({severity: 'error', summary: 'Operation Aborted', detail: 'New Notice was not published'});
            break;
          case ConfirmEventType.CANCEL:
            this.messageService.add({severity: 'warn', summary: 'Cancelled', detail: 'Publication of New notice as aborted'});
            break;
        }
      }
    });
  }

  publishNotice(){
   this.post.content = this.Post.value;
   console.log(this.post.content);
   const publish = this.noticeservice.submitNotice(this.post.content);
   // this use of suscribe method is generating an TypeError
   publish.subscribe((data) => {
     console.log(data);
     // this.messageService.add({severity: 'info', summary: 'Notice Published', detail: 'Your notice has been sent through email'});
   });
   /*
   this.noticeservice.submitNotice(this.post.content).subscribe(reply =>
   {
     console.log(reply);
     // tslint:disable-next-line:max-line-length
   // tslint:disable-next-line:max-line-length
   // tslint:disable-next-line:max-line-length
   //  this.messageService.add({severity: 'info', summary: 'Notice Published', detail:
   'The notice has been forwarded to all the employees.'});
   });
    */
  }
}
