import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitionEmployeeComponent } from './requisition-employee.component';

describe('RequisitionEmployeeComponent', () => {
  let component: RequisitionEmployeeComponent;
  let fixture: ComponentFixture<RequisitionEmployeeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequisitionEmployeeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitionEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
