import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { DefualtModule } from './layouts/defualt/defualt.module';
import {SharedModule} from './shared/shared.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {EmployeeModule} from './layouts/employee/employee.module';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import { LeaveComponent } from './modules/leave/leave.component';
import { RequisitionComponent } from './modules/requisition/requisition.component';
import { RequisitionEmployeeComponent } from './modules/requisition-employee/requisition-employee.component';
import {MatIconModule} from '@angular/material/icon';
import {StepsModule} from 'primeng/steps';
import {MenuItem} from 'primeng/api';
import {MatMenuItem} from '@angular/material/menu';
// import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';
import { CKEditorModule} from '@ckeditor/ckeditor5-angular';
import { ChipModule } from 'primeng/chip';
import {PickListModule} from 'primeng/picklist';

import {DialogModule} from 'primeng/dialog';
import { Step1Component } from './modules/leave/step1/step1.component';
import { Step2Component } from './modules/leave/step2/step2.component';
import { Step3Component } from './modules/leave/step3/step3.component';
@NgModule({
  declarations: [
    AppComponent,
    Step1Component,
    Step2Component,
    Step3Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    DefualtModule,
    EmployeeModule,
    SharedModule,
    MatSidenavModule,
    MatDividerModule,
    MatCardModule,
    MatIconModule,
    HttpClientModule,
    CKEditorModule,
    ChipModule,
    PickListModule,
    StepsModule,
    DialogModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
