import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import HC_exporting from 'highcharts/modules/exporting';
@Component({
  selector: 'app-widget-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {
  // @ts-ignore
  chartOptions: {};
  // formatter: number;
  Highcharts = Highcharts;
  constructor() { }
  // ngOnInit(): void {}
  ngOnInit(): void {
   // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    // @ts-ignore
    this.chartOptions = {
     chart: {
       type: 'area'
     },
     title: {
       text: 'Appointment per branch'
     },
     subtitle: {
       text: 'Classified data'
     },
     tooltip: {
       split: true,
       valueSuffix: '  Appointments'
     },
      credits: {
       enabled: false,
      },
     exporting: {
       enabled: true,
     },
     series: [{
       name: 'General',
       data: [502, 635, 809, 947, 1402, 3634, 5268]
     }, {
       name: 'Orthopedic',
       data: [106, 107, 111, 133, 221, 767, 1766]
     }, {
       name: 'Emergency',
       data: [163, 203, 276, 408, 547, 729, 628]
     }, {
       name: 'Gynecological',
       data: [18, 31, 54, 156, 339, 818, 1201]
     }, {
       name: 'Dental',
       data: [2, 2, 2, 6, 13, 30, 46]
     }]
   };
    HC_exporting(Highcharts);
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }

}
