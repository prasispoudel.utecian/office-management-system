import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HeaderComponent} from './components/header/header.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {MatDividerModule} from '@angular/material/divider';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from '../app-routing.module';
import { AreaComponent } from './widgets/area/area.component';
import {HighchartsChartModule} from 'highcharts-angular';
import { CardComponent } from './widgets/card/card.component';
// @ts-ignore
import { EmployeeFooterComponent } from './components/employee-footer/employee-footer.component';
import { EmployeeSidebarComponent } from './components/employee-sidebar/employee-sidebar.component';
import { EmployeeHeaderComponent } from './components/employee-header/employee-header.component';
import {MatCalendar} from '@angular/material/datepicker';
import {HttpClient} from '@angular/common/http';
import {ChipModule} from 'primeng/chip';
import {JwtModule, JwtModuleOptions} from '@auth0/angular-jwt';
import {combineAll} from 'rxjs/operators';

function yourTokenGetter() {
  console.log('Token Getter is being called');
  return localStorage.getItem('Token');
}

// tslint:disable-next-line:variable-name prefer-const
let yourWhitelistedDomains;

// tslint:disable-next-line:variable-name
const JWT_Module_Options: JwtModuleOptions = {
  config: {
    // @ts-ignore
    tokenGetter: yourTokenGetter,
    // @ts-ignore
    whitelistedDomains: yourWhitelistedDomains
  }
};
@NgModule({
  declarations: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    AreaComponent,
    CardComponent,
    EmployeeFooterComponent,
    EmployeeSidebarComponent,
    EmployeeHeaderComponent
  ],
  imports: [
    CommonModule,
    MatDividerModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    MatMenuModule,
    MatListModule,
    RouterModule,
    AppRoutingModule,
    HighchartsChartModule,
    ChipModule,
    JwtModule.forRoot(JWT_Module_Options)

  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    AreaComponent,
    CardComponent,
    EmployeeFooterComponent,
    EmployeeHeaderComponent,
    EmployeeSidebarComponent
  ]

})
export class SharedModule { }
