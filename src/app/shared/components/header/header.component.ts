import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router, RouterLink, RouterLinkActive} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() toogleSideBarForMe: EventEmitter<any> = new EventEmitter();
  url = 'assets/User_icon.png';
  constructor(private router: Router) { }
  logout(){
    console.log(localStorage.getItem('Token'));
    localStorage.removeItem('Token');
    console.log(localStorage.getItem('Token') + 'After');
    this.router.navigate(['']);
  }
  ngOnInit(): void {}
  // tslint:disable-next-line:typedef
  toogleSideBar(){
    // emitting the side bar event
    this.toogleSideBarForMe.emit();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }


}
